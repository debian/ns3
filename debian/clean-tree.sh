#!/bin/sh

#This script is used clean the source tree,after dpkg-buildpackage -rfakeroot 

#COPYRIGHT (C) YunQiang Su <wzssyqa@gmial.com>,2010.

#PUBILIC UNDER GPLv2 or LATER VERSION GPL.

#rm ./*.log

TOP_DIR=`pwd`
NS3_DIR=$TOP_DIR/`ls -d ns-*`
PYBINDGENV_DIR=$TOP_DIR/`ls -d pybindgen-0.*`
DEBIAN_DIR=$TOP_DIR/debian

echo $DEBIAN_DIR

###############################################
#clean top dir
cd $TOP_DIR
rm -f *.pyc *-stamp

################################################
#clean ns3 dir
cd $NS3_DIR

rm -f ./doc/introspected-doxygen.h ./doc/ns3-object.txt \
      ./doc/doxygen.warnings.log ./doc/ns3_html_theme/static/ns3_version.js
rm -rf ./doc/html \
       ./doc/latex

make clean -C ./doc/manual
make clean -C ./doc/tutorial
make clean -C ./doc/models

rm -f ./different.pcap ./*.txt

./waf clean
./waf distclean

./bindings/python/waf clean
./bindings/python/waf distclean

rm -rf ./build-*

find . -name '*.pyc' -exec rm {} \;
find . -name '.waf-*' -exec rm -rf {} \;

cd $PYBINDGENV_DIR
find . -name '*.pyc' -exec rm {} \;
find . -name '.waf-*' -exec rm -rf {} \;

exit 0
