#!/bin/sh

TMP1=doc-md5
TMP2=doc-md5.sort
CURDIR=$(pwd)
cd $1
find -maxdepth 1 -type f -exec md5sum {\} \; > $TMP1
sort $TMP1 > $TMP2

python ${CURDIR}/debian/duplicate-files.py $TMP2
rm -rf $TMP1 $TMP2
