FILES := $(shell find ns-3.*/doc/html/ -type f -name '*_*_*.png' -printf "%p\t")
TARGETS := $(FILES:=-update)
all: $(TARGETS)

%-update: %
	optipng -preserve -quiet -fix -o 5 $<

.PHONY: all
